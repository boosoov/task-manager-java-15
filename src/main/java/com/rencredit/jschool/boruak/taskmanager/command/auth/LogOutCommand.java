package com.rencredit.jschool.boruak.taskmanager.command.auth;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;

public class LogOutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String description() {
        return "Logout system.";
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().logOut();
        System.out.println("Have been logout");
    }

}
