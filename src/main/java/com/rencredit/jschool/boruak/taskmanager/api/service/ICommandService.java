package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.dto.Command;

import java.util.Map;

public interface ICommandService {

    Map<String, AbstractCommand> getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

    void registryCommand();

}
