package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ICommandRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.ServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.command.auth.LogOutCommand;
import com.rencredit.jschool.boruak.taskmanager.command.auth.LoginCommand;
import com.rencredit.jschool.boruak.taskmanager.command.project.*;
import com.rencredit.jschool.boruak.taskmanager.command.system.*;
import com.rencredit.jschool.boruak.taskmanager.command.task.*;
import com.rencredit.jschool.boruak.taskmanager.command.user.EditProfileCommand;
import com.rencredit.jschool.boruak.taskmanager.command.user.RegistrationCommand;
import com.rencredit.jschool.boruak.taskmanager.command.user.UpdatePasswordCommand;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

public class CommandRepository implements ICommandRepository {

    private final ServiceLocator serviceLocator;

    public CommandRepository(final ServiceLocator serviceLocator) {
        System.out.println("CommandRepository - ServiceLocator");
        System.out.println(serviceLocator);
        this.serviceLocator = serviceLocator;
    }

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void registryCommand(){
        registry(new HelpCommand());
        registry(new SystemInfoCommand());
        registry(new ShowArgumentsCommand());
        registry(new ShowCommandsCommand());
        registry(new ShowDeveloperInfoCommand());
        registry(new ShowVersionCommand());
        registry(new SystemExitCommand());
        registry(new UpdatePasswordCommand());
        registry(new ShowProfileCommand());
        registry(new EditProfileCommand());
        registry(new LogOutCommand());
        registry(new LoginCommand());
        registry(new RegistrationCommand());

        registry(new ClearProjectsCommand());
        registry(new CreateProjectCommand());
        registry(new RemoveProjectByIdCommand());
        registry(new RemoveProjectByIndexCommand());
        registry(new RemoveProjectByNameCommand());
        registry(new ShowProjectByIdCommand());
        registry(new ShowProjectByIndexCommand());
        registry(new ShowProjectByNameCommand());
        registry(new ShowProjectsCommand());
        registry(new UpdateProjectByIdCommand());
        registry(new UpdateProjectByIndexCommand());

        registry(new ClearTasksCommand());
        registry(new CreateTaskCommand());
        registry(new RemoveTaskByIdCommand());
        registry(new RemoveTaskByIndexCommand());
        registry(new RemoveTaskByNameCommand());
        registry(new ShowTaskByIdCommand());
        registry(new ShowTaskByIndexCommand());
        registry(new ShowTaskByNameCommand());
        registry(new ShowTasksCommand());
        registry(new UpdateTaskByIdCommand());
        registry(new UpdateTaskByIndexCommand());
    }

    private void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(serviceLocator);
        commands.put(command.name(), command);
    }

    public String[] getCommands() {
        final String[] result = new String[commands.size()];
        int index = 0;
        for (Map.Entry<String, AbstractCommand> command : commands.entrySet()){
            final StringBuilder resultString = new StringBuilder();
            if (command.getValue().name() != null && !command.getValue().name().isEmpty()) resultString.append(command.getValue().name());
            if (command.getValue().description() != null && !command.getValue().description().isEmpty()) resultString.append(": ").append(command.getValue().description());
            result[index] = resultString.toString();
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs() {
        final String[] result = new String[commands.size()];
        int index = 0;
        for (Map.Entry<String, AbstractCommand> command : commands.entrySet()){
            final StringBuilder resultString = new StringBuilder();
            if (command.getValue().arg() != null && !command.getValue().arg().isEmpty()) resultString.append(command.getValue().arg());
            if (command.getValue().description() != null && !command.getValue().description().isEmpty()) resultString.append(": ").append(command.getValue().description());
            result[index] = resultString.toString();
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public Map<String, AbstractCommand> getTerminalCommands() {
        return commands;
    }

}
