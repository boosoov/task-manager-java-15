package com.rencredit.jschool.boruak.taskmanager.util;

import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyHashLineException;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class HashUtil {

    private static final String SEPARATOR_KEY = "ghdgh453ng673";

    private static final int ITERATOR_KEY = 27;

    private HashUtil() {}

    public static String getHashLine(final String line) {
        return saltHashLine(line);
    }

    private static String saltHashLine(final String line) {
        if (line == null || line.isEmpty()) throw new EmptyHashLineException();
        String hashLine = line;
        for (int i = 0; i < ITERATOR_KEY; i++) {
            hashLine = hashLineMD5(SEPARATOR_KEY + line + SEPARATOR_KEY);
        }
        return hashLine;
    }

    private static String hashLineMD5(final String line) {
        if (line == null || line.isEmpty()) throw new EmptyHashLineException();
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            final byte[] byteArray = messageDigest.digest(line.getBytes(StandardCharsets.UTF_8));
            final StringBuilder stringBuilder = new StringBuilder();
            for (byte symbol : byteArray) {
                stringBuilder.append(Integer.toHexString((symbol & 0xFF) | 0x100), 1, 3);
            }
            return stringBuilder.toString();
        } catch (NoSuchAlgorithmException ex) {
            ex.getStackTrace();
        }
        return null;
    }

}
