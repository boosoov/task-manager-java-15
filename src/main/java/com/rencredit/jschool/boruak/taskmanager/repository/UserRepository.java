package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IUserRepository;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class UserRepository implements IUserRepository {

    private List<User> users = new ArrayList<>();

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public User findById(final String id) {
        for (User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findByLogin(final String login) {
        for (User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User removeById(final String id) {
        final User user = findById(id);
        if (Objects.isNull(user)) throw new EmptyUserException();
        return removeByUser(user);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (Objects.isNull(user)) throw new EmptyUserException();
        return removeByUser(user);
    }
    @Override
    public User removeByUser(final User user) {
        return (users.remove(user) ? user : null);
    }

}
