package com.rencredit.jschool.boruak.taskmanager.command.task;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;

public class ClearTasksCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        serviceLocator.getTaskService().clear(serviceLocator.getAuthService().getUserId());
        System.out.println("[OK]");
    }

}
