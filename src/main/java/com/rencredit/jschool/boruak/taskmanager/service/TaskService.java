package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ITaskRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.ITaskService;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        final Task task = new Task(name);
        taskRepository.add(userId, task);
    }

    @Override
    public void create(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();

        final Task task = new Task(name, description);
        taskRepository.add(userId, task);
    }

    @Override
    public void create(final String userId, final Task task) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) throw new EmptyTaskException();

        taskRepository.add(userId, task);
    }

    @Override
    public void remove(final String userId, final Task task) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) throw new EmptyTaskException();

        taskRepository.remove(userId, task);
    }

    @Override
    public List<Task> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        return taskRepository.findAll(userId);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        taskRepository.clear(userId);
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();

        return taskRepository.findOneByIndex(userId, index);
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return taskRepository.findOneByName(userId, name);
    }

    @Override
    public Task updateTaskById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        final Task task = findOneById(userId, id);
        if (task == null) throw new EmptyTaskException();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new EmptyTaskException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();

        return taskRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Task removeOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        return taskRepository.removeOneByName(userId, name);
    }

    @Override
    public Task findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        return taskRepository.findOneById(userId, id);
    }

    @Override
    public Task removeOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        return taskRepository.removeOneById(userId, id);
    }

}
