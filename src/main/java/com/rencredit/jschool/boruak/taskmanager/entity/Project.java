package com.rencredit.jschool.boruak.taskmanager.entity;

import java.util.UUID;

public class Project extends AbstractEntity {

    private String name = "";

    private String description = "";

    private String userId = "";

    public Project() {
    }

    public Project(final String name) {
        this.name = name;
    }

    public Project(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}
